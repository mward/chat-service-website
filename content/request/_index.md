
---
title: "Helpdesk request template"
date: 2020-03-01T16:09:45-04:00
#headline: "The Community for Open Innovation and Collaboration"
#tagline: "The Eclipse Foundation provides our global community of individuals and organizations with a mature, scalable, and business-friendly environment for open source software collaboration and innovation."
hide_page_title: true
#hide_sidebar: true
#hide_breadcrumb: true
#show_featured_story: true
layout: "request"
#links: [[href: "/projects/", text: "Projects"],[href: "/org/workinggroups/", text: "Working Group"],[href: "/membership/", text: "Members"],[href: "/org/value", text: "Business Value"]]
#container: "container-fluid"
---


# Request template for Eclipse Foundation helpdesk

Please use this template in order to create issue related to chat service room/space management in the Eclipse Foundation [helpdesk](https://gitlab.eclipse.org/eclipsefdn/helpdesk/-/issues/new?issue[title]=%5BChat%20service%5D%20room%2Fspace%20creation) 

## Title 

Title pattern: `[Chat service] Create room and space ...` 

## Description

{{< highlight go "style=github" >}}
# 💬 Request for rooms/spaces in the chat service
## 🌟 Project/working group reference

Please add a link to the project or working group to provide context.
e.g. for oniro-core project: https://projects.eclipse.org/projects/oniro.oniro-core


## 🚀 Room creation

**NOTE 1**: All eclipse project room will be attached to [#eclipse-projects:matrix.eclipse.org](https://chat.eclipse.org/#/room/#eclipse:matrix-staging.eclipse.org) space.

**NOTE 2**: Repeat as often as there are rooms to create.

**NOTE3**: Default values come from Eclipse projects API: `https://projects.eclipse.org/api/projects`.(`#project_id`, `#name`, `#summary`, `#logo` are attributs from the API response).

For instance, `mylyn project`: `https://projects.eclipse.org/api/projects/tools.mylyn.json`

You can request custom values for all of these attributes, based on the room/space organization desired by the project and validated by Eclipse staff.

|                      | Description                                                  | Data                                |
|----------------------|--------------------------------------------------------------|-------------------------------------|
| 🏠 Room name        | Desired room name (default: `#project_id:matrix.eclipse.org`, i.e: `tools.mylyn:matrix-staging.eclipse.org`)                   |                                     |
| 🏠 Room alias        | Desired room (default, `#name`, i.e: `Eclipse Mylyn`) alias                                           |                                     |
| 🎯 Room topic        | Briefly describe space topic (default, `#summary`, i.e: `Eclipse Mylyn is a Task-Focused Interface fo...`                               |                                     |
| 🌟 Room logo         | Upload or add to the issue a link to a logo. (by default, `#logo`)                |                                     |
| 👥 Room Moderators   | List of users designated to moderate the rooms. Only project lead or working group manager. | NOT YET AVAILABLE!     |

## 🌖 Space creation (Optional)

**NOTE**: Repeat as many times as there are spaces

You can ask for custom values about all this attributs, depending on the project room/space organization. 

|                    | Description                                                 | Data                                  |
|--------------------|-------------------------------------------------------------|---------------------------------------|
| 🏠 Space name      | Desired space name                |                                       |
| 🏠 Space alias     | Desired space alias                                         |                                       |
| 🎯 Space topic     | Briefly describe space topic                                |                                       |
| 🌟 Space logo      | Upload or add to the issue a link to the logo               |                                       |
| 👥 Space Moderators| List of users designated to moderate the spaces (mxid)      |  NOT YET AVAILABLE!                   |
| 🧩 Room attached   | List of rooms attached to this space                        |                                       |  


## 📝 Room/space modification

|                            | Description                                                 | Data                                  |
|----------------------------|-------------------------------------------------------------|---------------------------------------|
| 🏠 Room/space name         | Actual room/space name                                      | CAN'T BE RENAME!                      |
| 🎯 Room/space alias       | New room/space alias                                         |                                       |
| 🎯 Room/space topic        | New room/space topic                                        |                                       |
| 🌟 Room/space logo         | New upload logo or add a link to a logo                     |                                       |
| 👥 Room/space Moderators   | New list of users designated to moderate the rooms (mxid)   | NOT YET AVAILABLE!                    |
| 🧩 Space attached          | new space to attach the room                                |                                       |  


## 🗑️ Room/space deletion

|                      | Data                     | 
|----------------------|--------------------------|
| 🏠 Room/space name   |                          |


## 🚨 Priority

- [ ] Urgent
- [ ] High
- [ ] Medium
- [x] Low

## ⚠️ Severity

- [ ] Blocker
- [ ] Major
- [ ] Normal
- [x] Low

## 💥 Impact

What is the impact of this issue? Are there any time constraints?

{{< / highlight >}}

# Result of the issue markdown template
-------------------------------------------------------------------------------------
# 💬 Request for rooms/spaces in the chat service
## 🌟 Project/working group reference

Please add a link to the project or working group to provide context.
e.g. for oniro-core project: https://projects.eclipse.org/projects/oniro.oniro-core


## 🚀 Room creation

**NOTE 1**: All eclipse project room will be attached to [#eclipse-projects:matrix.eclipse.org](https://chat.eclipse.org/#/room/#eclipse:matrix-staging.eclipse.org) space.

**NOTE 2**: Repeat as often as there are rooms to create.

**NOTE3**: Default values come from Eclipse projects API: `https://projects.eclipse.org/api/projects`.(`#project_id`, `#name`, `#summary`, `#logo` are attributs from the API response).

For instance, `mylyn project`: `https://projects.eclipse.org/api/projects/tools.mylyn.json`

You can request custom values for all of these attributes, based on the room/space organization desired by the project and validated by Eclipse staff.

|                      | Description                                                  | Data                                |
|----------------------|--------------------------------------------------------------|-------------------------------------|
| 🏠 Room name        | Desired room name (default: `#project_id:matrix.eclipse.org`, i.e: `tools.mylyn:matrix-staging.eclipse.org`)                   |                                     |
| 🏠 Room alias        | Desired room (default, `#name`, i.e: `Eclipse Mylyn`) alias                                           |                                     |
| 🎯 Room topic        | Briefly describe space topic (default, `#summary`, i.e: `Eclipse Mylyn is a Task-Focused Interface fo...`                               |                                     |
| 🌟 Room logo         | Upload or add to the issue a link to a logo. (by default, `#logo`)                |                                     |
| 👥 Room Moderators   | List of users designated to moderate the rooms. Only project lead or working group manager. | NOT YET AVAILABLE!     |

## 🌖 Space creation (Optional)

**NOTE**: Repeat as many times as there are spaces

You can ask for custom values about all this attributs, depending on the project room/space organization. 

|                    | Description                                                 | Data                                  |
|--------------------|-------------------------------------------------------------|---------------------------------------|
| 🏠 Space name      | Desired space name                |                                       |
| 🏠 Space alias     | Desired space alias                                         |                                       |
| 🎯 Space topic     | Briefly describe space topic                                |                                       |
| 🌟 Space logo      | Upload or add to the issue a link to the logo               |                                       |
| 👥 Space Moderators| List of users designated to moderate the spaces (mxid)      |  NOT YET AVAILABLE!                   |
| 🧩 Room attached   | List of rooms attached to this space                        |                                       |  


## 📝 Room/space modification

|                            | Description                                                 | Data                                  |
|----------------------------|-------------------------------------------------------------|---------------------------------------|
| 🏠 Room/space name         | Actual room/space name                                      | CAN'T BE RENAME!                      |
| 🎯 Room/space alias       | New room/space alias                                         |                                       |
| 🎯 Room/space topic        | New room/space topic                                        |                                       |
| 🌟 Room/space logo         | New upload logo or add a link to a logo                     |                                       |
| 👥 Room/space Moderators   | New list of users designated to moderate the rooms (mxid)   | NOT YET AVAILABLE!                    |
| 🧩 Space attached          | new space to attach the room                                |                                       |  


## 🗑️ Room/space deletion

|                      | Data                     | 
|----------------------|--------------------------|
| 🏠 Room/space name   |                          |


## 🚨 Priority

- [ ] Urgent
- [ ] High
- [ ] Medium
- [x] Low

## ⚠️ Severity

- [ ] Blocker
- [ ] Major
- [ ] Normal
- [x] Low

## 💥 Impact

What is the impact of this issue? Are there any time constraints?