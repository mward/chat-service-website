---
title: "Frequently Asked Questions"
date: 2020-03-01T16:09:45-04:00
#headline: "The Community for Open Innovation and Collaboration"
#tagline: "The Eclipse Foundation provides our global community of individuals and organizations with a mature, scalable, and business-friendly environment for open source software collaboration and innovation."
hide_page_title: true
#hide_sidebar: true
#hide_breadcrumb: true
#show_featured_story: true
layout: "single"
#links: [[href: "/projects/", text: "Projects"],[href: "/org/workinggroups/", text: "Working Group"],[href: "/membership/", text: "Members"],[href: "/org/value", text: "Business Value"]]
#container: "container-fluid"
---

# Chat Service Frequently Asked Questions

From troubleshooting common issues to learning how to use various chat features, we've got you covered. If you can't find what you're looking for, please don't hesitate to reach out to our support team for further assistance.

You can access our support service by asking on the [#eclipsefdn.chat-support:matrix-staging.eclipse.org](https://chat-staging.eclipse.org/#/room/#eclipsefdn.chat-support:matrix-staging.eclipse.org) room, or by opening an issue in our [HelpDesk]([helpdesk](https://gitlab.eclipse.org/eclipsefdn/helpdesk/-/issues/new?issue[title]=%5BChat%20service%5D%20room%2Fspace%20creation) ).

Check our [support documentation page]({{< relref "/support" >}}) for more details!





## {{<heading>}} Using accounts from federated servers {{</heading>}}

**Q: Can I participate in a room with my account from matrix.org or any other matrix server?**

A: Yes, existing Matrix users can participate in our chat rooms and spaces with their current account from any other matrix server via federation and with any client of their choice.

By using our chat service, Eclipse members can easily authenticate using their Eclipse Foundation account. We encourage all Eclipse members to use our chat service as their primary point of entry to our Matrix server, but we welcome all those who share our communities passion for professional, collegal and respectful discussions about open source.

For those who don't have an Eclipse Foundation account, you can participate in our chat rooms and spaces by simply searching for the room or space you're interested in and joining as you would normally. 

## {{<heading>}} Phone number setup error {{</heading>}}

**Q:Why can't I setup my phone number ?**

A: Currently our chat service instance is not configured to support SMS. However, it does support email for notification.

It's also worth noting that some Matrix clients may have built-in support for SMS or other mediums, even if the chat instance at the Eclipse Foundation does not. If you're using a third-party client, check the documentation or settings to see if SMS is supported.


## {{<heading>}} Room/space creation error{{</heading>}}

**Q: Why am I receiving a 403 error when trying to create a room or space in chat service?**

A: At this time creation of rooms and spaces is restricted.

**Q: What should I do if I need to create a room or space?**

A: If your Eclipse project does not have it's own room or space yet, you can request it by opening a [HelpDesk issue](https://gitlab.eclipse.org/eclipsefdn/helpdesk/-/issues/new).

For more information, see [How can I create a specific room/space?](http://localhost:1313/faq/#request-for-roomspace)

**Q: Can I still join existing rooms or spaces even if I cannot create new ones?**

A: Yes, you can still join existing rooms or spaces even if you cannot create new ones. However, some rooms or spaces you want to join may have additional restrictions on joining.


## {{<heading>}} Request for room/space {{</heading>}}

**Q: How can I create a specific room/space?**

A: You can't create by yourself a specific room or space, you must request directly via the helpdesk by using a template. Here are the steps:
* Create the issue in the [helpdesk](https://gitlab.eclipse.org/eclipsefdn/helpdesk/-/issues/new)
* Change Title issue to this format: `[Chat service] Create room and space ...` 
* Copy the template: https://chat-staging.eclipse.org/docs/request
* Paste template in description section
* And fill in all the necessary information
  
## {{<heading>}} Moderation role {{</heading>}}

**Q: Will users be given moderation rights on the chat service?**
A: No, users will not be given moderation rights on this first version of the chat service. But we are studying the possibility of allowing an intermediate right between user and moderator.

**Q: Who will handle moderation on the chat platform?**
A: Moderation will be handled first by admin. 

**Q: Will users have any input in the moderation process?**
A: While users will not have moderation rights, they will be able to report inappropriate content or behavior to administrator. This will help to identify and address any issues that arise on the chat service.

**Q: What actions will be taken against users who violate the chat service policies?**
A: Users who violate the chat service code of conduct may be subject to a range of consequences, depending on the severity of the violation. These may include warnings, temporary or permanent bans.

## {{<heading>}} Reporting bad content {{</heading>}}

**Q: How can I report bad content on the chat service?**

A: To report bad content on Matrix, you can use the "Report" feature in from your client. This will allow you to report a specific message or user for inappropriate behavior or content to administrators. Alternatively, helpdesk can be used for this purpose.

**Q: What kind of content can I report?**

A: You can report any content that violates the Matrix server's terms of service or code of conduct. This includes messages or files that contain hate speech, harassment, or other inappropriate content.

**Q: What happens after I report bad content?**

A: After you report bad content, administrators will review the report and take appropriate action. This may include removing the content or taking action against the user who posted it.

**Q: What should I do if I receive a warning or ban for bad content?**

A: If you receive a warning or ban for bad content, you should review the Matrix server's terms of service and code of conduct to understand why your behavior was deemed inappropriate. If you believe the warning or ban was a mistaken, you can contact administrators.

**Q: How can I avoid posting bad content on Matrix?**

A: The best way to avoid posting bad content is to familiarize yourself with the server's terms of service and code of conduct, and to use common sense and good judgment when posting messages or files. If you're not sure whether a message or file is appropriate, it's better to not post it.

## {{<heading>}} Private room {{</heading>}}

**Q: Is it possible to create private rooms in the Eclipse Foundation's chat service?**
A: No, it is not possible to create private rooms in the Eclipse Foundation's chat service in accordance with Eclipse principles.

**Q: Why is it not possible to create private rooms in the Eclipse Foundation's chat service?**
A: The Eclipse Foundation's commitment to openness and transparency means that private rooms are not consistent with its principles. All conversations in the chat service should be visible to all members of the community to ensure transparency and encourage collaboration.

## {{<heading>}} Encrypted messages are not readable {{</heading>}}

**Q: Why am I seeing "Messages not readable" errors?**

A: You may see this error if you have received messages that were sent in an encrypted format, and your device or Element Web instance does not have the necessary encryption keys to decrypt and display the messages. To fix this, you can log in to your Matrix account on the device or instance where the messages were originally sent or by contacting the sender to request that they resend the messages in an unencrypted format.

**Q: How can I prevent "Messages not readable" errors in the future?**

A: Ensure that your device or Element Web instance has the necessary encryption keys to decrypt messages sent in encrypted or "not readable" format. Additionally, you should ensure that your device or instance is up-to-date and running the latest version of Element Web to avoid any compatibility issues.

**Q: Why can't I post an encrypted message to a room?**

A: The chat service hosted at the Eclipse Foundation only allows encrypted messages for 1:1 communication. All rooms are public and not encrypted.

## {{<heading>}} Media storage restriction {{</heading>}}

**Q: What is the maximum media size that can be shared on chat service?**

A: The maximum media size that can be shared or uploaded as media, such as images or videos on the chat service is 100MB per upload with a quotas of 50GB per user.

**Q: Why are there media size restrictions in chat service?**

A: Media size restrictions are in place to prevent the overuse of server resources and to ensure that Matrix remains a fast and reliable communication platform for all users.

**Q: What happens if I try to upload a media file that exceeds the size limit?**

A: If you try to upload a media file that exceeds the size limit, you may receive an error message or the upload may fail. 

**Q: What should I do if I need to share a large media file on chat service but it exceeds the size limit?**

A:  You may want to consider using a third-party service to host the file and sharing the link in Matrix. Alternatively, you can contact the support to see if there are any alternative solutions available.

## {{<heading>}} Is our server on your federation blacklist? {{</heading>}}

**Q: How can I determine if my Matrix server is blacklisted?**

A: Currently we have no server on our federation blacklist. This cannot be the reason for your federation problem.

## {{<heading>}} Matrix client support other than element-web hosted at Eclipse {{</heading>}}

**Q: Can the support team help with issues related to other Matrix clients?**

A: We regret to inform you that we cannot provide support for any issues that arise from using other Matrix clients.

## {{<heading>}} Video/voice call {{</heading>}}

**Q: Can I make voice and video calls using Element Web on the Eclipse Foundation chat service?**

A: No, voice and video calls are currently not supported on the Eclipse Foundation chat service using Element Web.

**Q: Will voice and video calls be supported in the future?**

A: The development team is continuously working to improve and enhance the features of the chat service. While we cannot guarantee that voice and video calls will be supported in the future, we will continue to listen to feedback from our community and prioritize features accordingly.

## {{<heading>}} Communicating with other protocols(Slack, Whatsapp, etc) {{</heading>}}

## {{<heading>}} Can I use bots? {{</heading>}}

**Q: Why doesn't the Eclipse Foundation currently implement bots in its chat service?**

A: The Eclipse Foundation has not implemented bridges or bots in its chat service for several reasons:
* Focus on core features: The Foundation is currently focused on improving the core features of its chat service, such as usability, performance, and security. This means that bots may not be a priority at this time.
* Compatibility: these features require continuous updates and maintenance to ensure that they are compatible with the chat service and other systems. The Eclipse Foundation may want to ensure that any bots or bridges it implements are reliable and can be maintained over time.
* Promoting decentralized communication: The Eclipse Foundation is committed to promoting decentralized communication through the Matrix protocol. By relying on bridges to connect with other messaging platforms, it could be seen as undermining this goal.
