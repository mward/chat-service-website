---
title: "Getting Started!"
date: 2020-03-01T16:09:45-04:00
#headline: "The Community for Open Innovation and Collaboration"
#tagline: "The Eclipse Foundation provides our global community of individuals and organizations with a mature, scalable, and business-friendly environment for open source software collaboration and innovation."
hide_page_title: true
#hide_sidebar: false
#hide_breadcrumb: true
#show_featured_story: true
layout: "single"
#links: [[href: "/projects/", text: "Projects"],[href: "/org/workinggroups/", text: "Working Group"],[href: "/membership/", text: "Members"],[href: "/org/value", text: "Business Value"]]
#container: "container-fluid"
---

# Getting Started!

## {{<heading>}} Access to the chat Service {{</heading>}}

Start by visiting the [Eclipse Foundation chat service homepage](https://chat-staging.eclipse.org).

To connect, you will need to [create account or sign in](https://accounts.eclipse.org/). If you already have an Eclipse Foundation account, you can use your existing credentials to sign in. If you do not have an Eclipse Foundation account yet, you can create one by following the registration process on the login page. Once you have signed in to Matrix for the first time, you'll receive an `mxid` a unique identifier used to represent a Matrix user account. It is in the format of `@username:chat-staging.eclipse.org`. After that, you will be able to join public rooms and start communicating with other users of the community.

With a wide range of rooms dedicated to individual Eclipse projects available, you'll be able to connect with people who share your interests and expertise, whether you're interested in software development, open source, or anything in between.

## {{<heading>}} Consent policy {{</heading>}}

When you first create an account on Eclipse Foundation chat service, you will be presented with the Eclipse Foundation's privacy policy, terms of use and code of conduct. You will need to review and accept these policies in order to use the service.

The privacy policy outlines how the Eclipse Foundation collects, uses, and protects your personal information, as well as your rights and choices regarding your data. The terms of use outline the rules and guidelines for using the Eclipse Foundation chat service, including prohibitions on abusive or inappropriate behavior, spamming, and other activities that could disrupt the community.

Once you have reviewed and accepted the privacy policy and terms of use, you will be able to use the Eclipse Foundation chat service with full access to all available features and functionalities. It's important to review these policies carefully and ensure that you understand and agree to them before using the service. If you have any questions or concerns about the policies or your account, you can contact [privacy@eclipse.org](mailto:privacy@eclipse.org). 


## {{<heading>}} Account configuration: Passphrase/recovery key {{</heading>}}

Passphrases and recovery keys are security measures that can help protect your account and data in our chat service, you will be asked to set them up when you first connect/login. 

A passphrase is a user-generated password that is used to encrypt your private keys, which are used to sign and decrypt messages. When you first login to the Eclipse Foundation chat service, you'll be prompted to create this passphrase. You should choose a passphrase that is both easy for you to remember and difficult for others to guess. If you forget your passphrase, you won't be able to access your private keys and your account will be lost.

A recovery key, on the other hand, is a backup that can be used to restore your account if you forget your passphrase or lose access to your private keys. When you first login to a new account, you'll also be given a recovery key. You should store this key somewhere safe. If you ever need to restore your account using the recovery key, you'll need to enter it exactly as it was given to you when you created your account.

In general, it's a good idea to use both a passphrase and a recovery key for maximum security. However, if you have to choose between the two, we recommend using a passphrase. A strong passphrase can protect your account from unauthorized access, and it's also easier to use on a daily basis than a recovery key. We recommend to use a password manager to store the passphrase and/or the recovery key.


## {{<heading>}} Organization {{</heading>}}

At the heart of the platform is the [Eclipse projects](https://chat-staging.eclipse.org/#/room/#eclipse:matrix-staging.eclipse.org) space. Within this space, each project has its own dedicated room.

In addition there is the [Eclipse Foundation space](https://chat-staging.eclipse.org/#/room/#eclipsefdn:matrix-staging.eclipse.org), for the Eclipse Foundation to communicate with the community.

If required, Eclipse projects and Working Groups can ask for a custom space. These spaces can contain multiple rooms allowing a better view of a complete ecosystem, enabling contributors to work together more effectively and efficiently.

Overall, the organization of spaces and rooms within the chat service is designed to provide a flexible and adaptable platform that can meet the needs of a wide range of users and projects. 

## {{<heading>}} Find and access room and space {{</heading>}}

Here are some tips for finding rooms and spaces for Eclipse projects:

1. Explore the Eclipse Foundation projects: the chat service has a built-in directory of Eclipse Foundation public rooms. You can access it by clicking on the "Explore rooms" button on the left-hand side of the app. From there, you can browse public rooms in various categories, including "Technology - Eclipse Foundation."

2. Search for rooms using keywords: If you have a specific Eclipse project or topic in mind, you can use the search feature to find relevant rooms/spaces. Simply click on the magnifying glass icon at the top of the app and enter your keywords in the search box. You can narrow your search by filtering results by room name, space, name, topic, and more.

3. Ask for recommendations: Don't hesitate to ask for recommendations from other members. They may be able to point you towards relevant rooms and spaces that you wouldn't have found otherwise.

4. Ask for support: Finally, consider asking in the [Eclipse Foundation support room](https://chat-staging.eclipse.org/#/room/#eclipsefdn.chat-support:matrix-staging.eclipse.org) if you haven't already or the [general room](https://chat-staging.eclipse.org/#/room/#eclipsefdn.general:matrix-staging.eclipse.org). The [general room](https://chat-staging.eclipse.org/#/room/#eclipsefdn.general:matrix-staging.eclipse.org) serves as a hub for all things Eclipse-related on the chat service, and members can share news and updates about projects they're working on. You may also find helpful links to other rooms and spaces related to Eclipse projects.


## {{<heading>}} Find and talk to project committers and contributors {{</heading>}}

You may be interested in speaking directly with committers or other contributors of a particular project. Here's how you can do that:

1. First, make sure that you are logged in to the chat service and have joined the relevant chat room where the project is being discussed. 

2. Next, navigate to the project's page at the [Eclipse Projects site](https://projects.eclipse.org/), to the tab Who's Involved; ie. with [Oniro Project](https://projects.eclipse.org/projects/oniro.oniro-core/who). This information may also be available in the project's README file or on their website.

3. Once you have identified the relevant committers or contributors, you can search for them within the chat room using the search feature. Simply type their name or username into the search bar, and the platform will display all matching results.

4. Click on the name of the committer or contributor that you wish to speak with, and you will be taken to their profile page within the chat room.

5. From there, you can send a direct message to the committer or contributor by clicking on the "Message" button within their profile. This will open a chat window where you can start typing your message and have a direct conversation with them.

By following these simple steps, you can search for and talk to committers and contributors on Chat Service, facilitating direct communication and collaboration. 

Note: 1:1 communication is encrypted by default. 


## {{<heading>}} Find and talk to Eclipse Foundation Staff {{</heading>}}

You can follow the same steps as above for searching Eclipse Foundation Staff. 

For example: If you wanted to talk to a member of the releng team, go to the [releng team room](https://chat-staging.eclipse.org/#/room/#eclipsefdn.releng:matrix-staging.eclipse.org) in eclipsefdn space and follow these steps:  


1. First, make sure that you are logged in and are a member of the relevant room where the person you are searching for is present.

2. Click on the room settings icon in the top of the screen. This icon is a drop down menu with the room name.

3. From the dropdown menu, select "People" to view a list of all members of the room.

4. Look for `Moderator` or `Admin`. The profile status is next to the name or type the name or username of the person you are searching for in the search bar, and the platform will display all matching results.

5. Once you have found the person you are looking for, you can send them a direct message by clicking on their name in the search results and selecting "Message" from the dropdown menu.

**Please note: Eclipse Foundation staff can only offer limited support in chat service rooms. For general support requests, please open a [HelpDesk issue](https://gitlab.eclipse.org/eclipsefdn/helpdesk/-/issues/new).**
